package br.com.consiste.business;

import java.util.List;

import br.com.consiste.domain.People;
import br.com.consiste.persistence.PeopleDAO;

public class PeopleBO {
	
	public static List<People> researchPeoples(){
		return PeopleDAO.read();
	}
	
	public static void getData() {
		PeopleDAO.run();
	}
	
	public static void createAndWriteOutput() {
		PeopleDAO.generateOutputFile();
	}
}
