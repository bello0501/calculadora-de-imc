package br.com.consiste.domain;

import java.io.Serializable;

public class People implements Serializable {
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private Double weight;
	private Double height;

	public People(String firstName, String lastName, Double weight, Double height) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.weight = weight;
		this.height = height;

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getImc() {
		return weight == null || height == null ? 0 : weight / (height * height);
	}

	@Override
	public String toString() {
		return firstName.toUpperCase() + " " + lastName.toUpperCase() + " " + String.format("%.2f", getImc());
	}

}
