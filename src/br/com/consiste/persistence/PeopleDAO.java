package br.com.consiste.persistence;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.consiste.domain.People;
import br.com.consiste.utils.FileUtils;

public class PeopleDAO {

	private static List<People> listPeoples = new ArrayList<People>();
	
	public static List<People> read() {
		return listPeoples;
	}
	
	public static void generateOutputFile() {
		FileUtils.createOutput();
		FileUtils.writeOutput(listPeoples);
	}
	public static void run() {
		String fileCSV = "dataset.csv";
		BufferedReader br = null;
		String line = "";
		String csvDivider = ";";
		try {

			br = new BufferedReader(new FileReader(fileCSV));
			int index = 0;
			while ((line = br.readLine()) != null) {

				String[] peoples = line.split(csvDivider);
				String firstName = null;
				String lastName = null;
				Double weight = null;
				Double height = null;

				if (index != 0) {
					for (int i = 0; i < peoples.length; i++) {
						if (i == 0) {
							firstName = peoples[i].replaceAll("\\s{2,}", " ").trim();
						} else if (i == 1) {
							lastName = peoples[i].replaceAll("\\s{2,}", " ").trim();
						} else if (i == 2) {
							weight = Double.parseDouble(peoples[i].replace(',', '.'));
						} else if (i == 3) {
							height = Double.parseDouble(peoples[i].replace(',', '.'));
						}
					}
					People people = new People(firstName, lastName, weight, height);
					listPeoples.add(people);
				}
				index++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
