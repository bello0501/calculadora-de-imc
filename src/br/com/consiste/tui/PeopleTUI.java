package br.com.consiste.tui;

import java.util.List;

import br.com.consiste.business.PeopleBO;
import br.com.consiste.domain.People;

public class PeopleTUI {
	public static void main(String[] args) {
		PeopleBO.getData();
		start();
	}

	private static void start() {
		printPeoples(getPeoples());
		getOutput();
	}
	
	private static List<People> getPeoples(){
		return PeopleBO.researchPeoples();
	}
	
	private static void printPeoples(List<People> listPeoples) {
		for(People people: listPeoples) {
			System.out.println(people.toString());
		}
	}
	
	private static void getOutput() {
		PeopleBO.createAndWriteOutput();
	}
}
