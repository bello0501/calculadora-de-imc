package br.com.consiste.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import br.com.consiste.domain.People;

public class FileUtils {
	public static void createOutput() {
		File output = new File("RicardoAndradeOliveiraBello.txt");
		try {
			output.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeOutput(List<People> listPeoples) {
		try {
			FileWriter outputWriter = new FileWriter("RicardoAndradeOliveiraBello.txt");
			String outputResult = "";
			for(People people: listPeoples) {
				outputResult += people.toString() + "\n";
			}
			outputWriter.write(outputResult);
			outputWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
